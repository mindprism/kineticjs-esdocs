var menu=[
  {t:'Rectangle',u:'001-rect.html',c:'Shapes'}
  ,{t:'Circle',u:'002-circle.html',c:'Shapes'}
  ,{t:'Ellipse',u:'003-ellipse.html',c:'Shapes'}
  ,{t:'Wedge',u:'004-wedge.html',c:'Shapes'}
  ,{t:'Line',u:'005-line.html',c:'Shapes'}
  ,{t:'Polygon',u:'006-polygon.html',c:'Shapes'}
  ,{t:'Image',u:'007-image.html',c:'Shapes'}
  ,{t:'Sprite',u:'008-sprite.html',c:'Shapes'}
  ,{t:'Text',u:'009-text.html',c:'Shapes'}
  ,{t:'Spline',u:'010-spline.html',c:'Shapes'}
  ,{t:'Blob',u:'011-blob.html',c:'Shapes'}
  ,{t:'Custom',u:'012-custom.html',c:'Shapes'}
  ,{t:'RegularPolygon',u:'013-regularpolygon.html',c:'Shapes'}
  ,{t:'Star',u:'014-star.html',c:'Shapes'}
  ,{t:'TextPath',u:'015-textpath.html',c:'Shapes'}
  ,{t:'Label',u:'016-label.html',c:'Shapes'}
  ,{t:'Fill',u:'017-fill.html',c:'Styling'}
  ,{t:'Stroke',u:'018-stroke.html',c:'Styling'}
  ,{t:'Opacity',u:'019-opacity.html',c:'Styling'}
  ,{t:'Shadow',u:'020-shadow.html',c:'Styling'}
  ,{t:'Line Join',u:'021-linejoin.html',c:'Styling'}
  ,{t:'Hide and Show',u:'022-hideshow.html',c:'Styling'}
  ,{t:'Binding Events',u:'023-bindingevents.html',c:'Events'}
  ,{t:'Image Events',u:'024-imageevents.html',c:'Events'}
  ,{t:'Mobile Events',u:'025-mobileevents.html',c:'Events'}
  ,{t:'Multi Events',u:'026-multievent.html',c:'Events'}
  ,{t:'Desktop/mobile',u:'027-desktopmobile.html',c:'Events'}
  ,{t:'Remove Event',u:'028-removeevent.html',c:'Events'}
  ,{t:'Remove by Name',u:'029-removebyname.html',c:'Events'}
  ,{t:'Custom Hit Region',u:'030-customhitregion.html',c:'Events'}
  ,{t:'Listen for Events',u:'031-listenforevents.html',c:'Events'}
  ,{t:'Cancel Propagation',u:'032-cancelpropagation.html',c:'Events'}
  ,{t:'Event Delegation',u:'033-eventdelegation.html',c:'Events'}
  ,{t:'Fire Events',u:'034-fireevents.html',c:'Events'}
  ,{t:'Drag and Drop',u:'035-dragdrop.html',c:'Drag &amp; Drop'}
  ,{t:'Drag an Image',u:'036-dragimage.html',c:'Drag &amp; Drop'}
  ,{t:'Drag a Group',u:'037-draggroup.html',c:'Drag &amp; Drop'}
  ,{t:'Drag a Line',u:'038-dragline.html',c:'Drag &amp; Drop'}
  ,{t:'Drag the Stage',u:'039-dragstage.html',c:'Drag &amp; Drop'}
  ,{t:'Drag Events',u:'040-dragevents.html',c:'Drag &amp; Drop'}
  ,{t:'Simple Drag Bounds',u:'041-simpledragbounds.html',c:'Drag &amp; Drop'}
  ,{t:'Complex Drag Bounds',u:'042-complexdragbounds.html',c:'Drag &amp; Drop'}
  ,{t:'Groups',u:'043-groups.html',c:'Groups &amp; Layers'}
  ,{t:'Layering',u:'044-layering.html',c:'Groups &amp; Layers'}
  ,{t:'Change Containers',u:'045-changecontainers.html',c:'Groups &amp; Layers'}
  ,{t:'Simple Transition',u:'046-simpletransition.html',c:'Transitions &amp; Easing'}
  ,{t:'Transition Easing',u:'047-transitioneasing.html',c:'Transitions &amp; Easing'}
  ,{t:'All Easing',u:'048-alleasing.html',c:'Transitions &amp; Easing'}
  ,{t:'Callback',u:'049-callback.html',c:'Transitions &amp; Easing'}
  ,{t:'Stop and Resume',u:'050-stopresume.html',c:'Transitions &amp; Easing'}
  ,{t:'Creating an Animation',u:'051-createanimation.html',c:'Animations'}
  ,{t:'Moving',u:'052-moving.html',c:'Animations'}
  ,{t:'Rotating',u:'053-rotating.html',c:'Animations'}
  ,{t:'Scaling',u:'054-scaling.html',c:'Animations'}
  ,{t:'Stop Animation',u:'055-stopanimation.html',c:'Animations'}
  ,{t:'Select by Id',u:'056-selectbyid.html',c:'Selecting'}
  ,{t:'Select by Name',u:'057-selectbyname.html',c:'Selecting'}
  ,{t:'Data URL',u:'058-dataurl.html',c:'Data &amp; Serialization'}
  ,{t:'Serialize Stage',u:'059-serializestage.html',c:'Data &amp; Serialization'}
  ,{t:'Simple Load',u:'060-simpleload.html',c:'Data &amp; Serialization'}
  ,{t:'Complex Load',u:'061-complexload.html',c:'Data &amp; Serialization'}
  ,{t:'Shape Cacheing',u:'062-shapecache.html',i:'test',c:'Preformance'}
];

var _kjs='<strong>KineticJS</strong> (<a href="http://www.kineticjs.com" target="_blank">tar</a>,<a href="http://mrobbinsassoc.com/projects/kineticjs/jsdoc/" target="kinetic">jsdoc</a>)';
//http://mrobbinsassoc.com/projects/kineticjs/jsdoc
//
//http://kineticjs.com/docs/symbols

function jsdocobj(n){
  var s='';
  s+='<a href="http://mrobbinsassoc.com/projects/kineticjs/jsdoc/'+n+'.html" target="kinetic"><code>'+n+'</code></a>';
  return s;
}
function tocreate(w,n,m,m2){
  m=m||'';
  m2=m2||'';
  var s='';
  s+='<p>To create a <i>'+w+'</i> '+m2+' with '+_kjs+', we can instantiate a '+jsdocobj(n)+' '+m+'</p>'
  return s;
}


function docs_str(l){
  if(!docs[l]) {
    return '';
  }
  var s='';
  if(typeof docs[l]==='object' && docs[l]!=null) {
    if(docs[l].w) {
      s+='<p><span class="warn"><b>WARNING:</b> '+docs[l].w+'</span></p>';
    }
    if(docs[l].i) {
      s+='<p><b>Instructions:</b> '+docs[l].i+'</p>';
    }
    if(docs[l].t) {
      s+=docs[l].t;
    }
    if(docs[l].n) {
      s+='<p><span class="note"><b>Note:</b>  '+docs[l].n+'</span></p>';
    }
    //<p><span class="note"><b>Note:</b> The <em>createImageHitRegion()</em> method requires that the image is hosted on a web server with the same domain as the code executing it.</span></p><p><b>Instructions:</b> Mouse over the monkey and the lion and observe the mouseover event bindings.&nbsp; Notice that the event is triggered for the monkey if you mouseover any portion of the image, including transparent pixels.&nbsp; Since we created an image hit region for the lion, transparent pixels are ignored, which enables more precise event detection.</p>
    //<p><b>Instructions:</b>
  }else if (typeof docs[l]==='string'){
    s+=docs[l];
  }
  return s;
}

function adapted(){
  return 'adapted from v4.4.3 ; in v5.1.1 <strong>Line</strong>, <strong>Spline</strong>, <strong>Polygon</strong>, and <strong>Blob</strong> have been combined into '+jsdocobj('Kinetic.Line')+'. A <strong>Spline</strong> is a '+jsdocobj('Kinetic.Line')+' with <strong>tension</strong>. A <strong>Polygon</strong> is a '+jsdocobj('Kinetic.Line')+' with <strong>closed=true</strong>. A <strong>Blob</strong> is a '+jsdocobj('Kinetic.Line')+' with <strong>tension</strong> and <strong>closed=true</strong>.';
}

function jsdoc_attr_s(c,a,t){
  t=t||a+'()';
  var c2=c;
  if(c=='Kinetic.Shape') {
    c2='';
  }
  return '<a href="http://mrobbinsassoc.com/projects/kineticjs/jsdoc/'+c+'.html#.'+a+'__anchor" target="kinetic"><code>'+c2+' '+t+'</code></a>';
}

function jsdoc_attr(c,a,t){
  t=t||a+'()';
  var c2=c;
  if(c=='Kinetic.Shape') {
    c2='';
  }
  return '<a href="http://mrobbinsassoc.com/projects/kineticjs/jsdoc/'+c+'.html#'+a+'__anchor" target="kinetic"><code>'+c2+' '+t+'</code></a>';
}

function stage_layer(){
  return jsdocobj('Kinetic.Stage')+', '
    +jsdoc_attr('Kinetic.Stage','add')+', '
    +jsdocobj('Kinetic.Layer')+', '
    +jsdoc_attr('Kinetic.Layer','add');
}

function kjs_events(){
  return ''+
    '<p>KineticJS supports'
    +' <em>mouseover</em>,'
    +' <em>mousemove</em>,'
    +' <em>mouseout</em>,'
    +' <em>mouseenter</em>,'
    +' <em>mouseleave</em>,'
    +' <em>mousedown</em>,'
    +' <em>mouseup</em>,'
    +' <em>mousewheel</em>,'
    +' <em>click</em>,'
    +' <em>dblclick</em>,'
    +' <em>touchstart</em>,'
    +' <em>touchmove</em>,'
    +' <em>touchend</em>,'
    +' <em>tap</em>,'
    +' <em>dbltap</em>,'
    +' <em>dragstart</em>,'
    +' <em>dragmove</em>,'+' and'
    +' <em>dragend</em>'
    +' events for shapes.</p>'
    +'<p>KineticJS supports'
    +' <em>contentMouseover</em>,'
    +' <em>contentMousemove</em>,'
    +' <em>contentMouseout</em>,'
    +' <em>contentMousedown</em>,'
    +' <em>contentMouseup</em>,'
    +' <em>contentClick</em>,'
    +' <em>contentDblclick</em>,'
    +' <em>contentTouchstart</em>,'
    +' <em>contentTouchmove</em>,'
    +' <em>contentTouchend</em>,'
    +' <em>contentTap</em>, and'
    +' <em>contentDblTap</em>'
    +' events for the stage.</p>';

}


function k_easings(){
  return ''+
  jsdoc_attr_s('Kinetic.Easings','Linear')+', '
  +jsdoc_attr_s('Kinetic.Easings','BackIn')+', '
  +jsdoc_attr_s('Kinetic.Easings','BackInOut')+', '
  +jsdoc_attr_s('Kinetic.Easings','BackOut')+', '
  +jsdoc_attr_s('Kinetic.Easings','BounceIn')+', '
  +jsdoc_attr_s('Kinetic.Easings','BounceInOut')+', '
  +jsdoc_attr_s('Kinetic.Easings','BounceOut')+', '
  +jsdoc_attr_s('Kinetic.Easings','EaseIn')+', '
  +jsdoc_attr_s('Kinetic.Easings','EaseInOut')+', '
  +jsdoc_attr_s('Kinetic.Easings','EaseOut')+', '
  +jsdoc_attr_s('Kinetic.Easings','ElasticIn')+', '
  +jsdoc_attr_s('Kinetic.Easings','ElasticInOut')+', '
  +jsdoc_attr_s('Kinetic.Easings','ElasticOut')+', '
  +jsdoc_attr_s('Kinetic.Easings','StrongIn')+', '
  +jsdoc_attr_s('Kinetic.Easings','StrongInOut')+', '
  +jsdoc_attr_s('Kinetic.Easings','StrongOut')+' '
}

var docs={
  '001-rect.html':{t:tocreate('rectangle','Kinetic.Rect')
    +'<p>See also: <small>'
    +stage_layer()
    +'</small></p>'
    }
  ,'002-circle.html':{t:tocreate('circle','Kinetic.Circle')
    +'<p>See also: <small>'
    +stage_layer()
    +'</small></p>'
    }
  ,'003-ellipse.html':{t:tocreate('ellipse','Kinetic.Ellipse')
    +'<p>See also: <small>'
    +stage_layer()
    +'</small></p>'
    }
  ,'004-wedge.html':{t:tocreate('wedge','Kinetic.Wedge')
    +'<p>See also: <small>'
    +stage_layer()
    +'</small></p>'
    }
  ,'005-line.html':{t:tocreate('line','Kinetic.Line')
    +'<p>See also: <small>'
    +jsdoc_attr('Kinetic.Line','dash')+', '
    +jsdoc_attr('Kinetic.Node','move')+', '
    +stage_layer()
    +'</small></p>'
    }
  ,'006-polygon.html':{t:tocreate('polygon','Kinetic.Line')
    +'<p>See also: <small>'
    +jsdoc_attr('Kinetic.Line','closed')+', '
    +stage_layer()
    +'</small></p>'
    ,w:adapted()
    }
  ,'007-image.html':{t:tocreate('image','Kinetic.Image')
    +'<p>See also: <small>'
    +jsdoc_attr('Kinetic.Image','onload')+', '
    +jsdoc_attr('Kinetic.Image','filters')+', '
    +jsdocobj('Kinetic.Filters')+', '
    +stage_layer()
    +'</small></p>'
    ,w:'I cannot seem to get filters working on images'
    }
  ,'008-sprite.html':{t:tocreate('animated sprite','Kinetic.Sprite')
    +'<p>See also: <small>'
    +jsdoc_attr('Kinetic.Sprite','start')+', '
    +jsdoc_attr('Kinetic.Sprite','animation')+', '
    +jsdoc_attr('Kinetic.Sprite','animations')+', '
    +jsdoc_attr('Kinetic.Sprite','frameRate')+', '
    +jsdoc_attr('Kinetic.Sprite','frameIndex')+', '
    +jsdoc_attr('Kinetic.Sprite','index')+', '
    +stage_layer()
    +'</small></p>'
    }
  ,'009-text.html':{t:tocreate('text','Kinetic.Text')
    +'<p>See also: <small>'
    +jsdoc_attr('Kinetic.Shape','offsetY')+', '
    +jsdoc_attr('Kinetic.Shape','width')+', '
    +stage_layer()
    +'</small></p>'
    }
  ,'010-spline.html':{t:tocreate('interpolated spline','Kinetic.Line', ' with '+jsdoc_attr('Kinetic.Line','tension'))
    +'<p>See also: <small>'
    +stage_layer()
    +'</small></p>'
    ,w:adapted()
    }
  ,'011-blob.html':{t:tocreate('blobs','Kinetic.Line', ' with '+jsdoc_attr('Kinetic.Line','tension')+' and '+ ' with '+jsdoc_attr('Kinetic.Line','closed'))
    +'<p>See also: <small>'
    +stage_layer()
    +'</small></p>'
    ,w:adapted()
    }
  ,'012-custom.html':{t:tocreate('custom shape','Kinetic.Shape',' and the '+jsdocobj('Kinetic.Canvas')+' object')
    +'<p>See also: <small>'
    +stage_layer()
    +'</small></p>'
    }
  ,'013-regularpolygon.html':{t:tocreate('regular polygon','Kinetic.RegularPolygon')
    +'<p>See also: <small>'
    +stage_layer()
    +'</small></p>'
    }
  ,'014-star.html':{t:tocreate('star','Kinetic.Star')
    +'<p>See also: <small>'
    +stage_layer()
    +'</small></p>'
    }
  ,'015-textpath.html':{t:tocreate('text along a path','Kinetic.TextPath')
    +'<p>See also: <small>'
    +stage_layer()
    +'</small></p>'
    }
  ,'016-label.html':{t:tocreate('text label','Kinetic.Label',' and a '+jsdocobj('Kinetic.Tag')+' and a '+jsdocobj('Kinetic.Text'),'which can be used for creating text with backgrounds, simple tooltips, or tooltips with pointers')
    +'<p>See also: <small>'
    +stage_layer()
    +'</small></p>'
    }
  ,'017-fill.html':{t:'<p>To <i>fill</i> a shape with '+_kjs+', we can set the <em>fill</em> property when we instantiate a shape, or we can use the '
    +jsdoc_attr('Kinetic.Shape','fill')+' or <em>setFill()</em> method.&nbsp; KineticJS supports colors, patterns, linear gradients, and radial gradients.</p>'
    +'<p>See also: <small>'
    +jsdocobj('Kinetic.RegularPolygon')+', '
    +jsdoc_attr('Kinetic.Shape','fillPatternImage')+', <em>setFillPatternImage()</em>, '
    +jsdoc_attr('Kinetic.Shape','fillPatternOffset')+', <em>setFillPatternOffset()</em>, '
    +jsdoc_attr('Kinetic.Shape','fillLinearGradientStartPoint')+', <em>setFillLinearGradientStartPoint()</em>, '
    +jsdoc_attr('Kinetic.Shape','fillLinearGradientEndPoint')+', <em>setFillLinearGradientEndPoint()</em>, '
    +jsdoc_attr('Kinetic.Shape','fillLinearGradientColorStops')+', <em>setFillLinearGradientColorStops()</em>, '
    +jsdoc_attr('Kinetic.Shape','fillRadialGradientStartRadius')+', <em>setFillRadialGradientStartRadius()</em>, '
    +'</small></p>'
    ,i:'Mouseover each pentagon to change its fill.&nbsp; You can also drag and drop the shapes.'
    }
  ,'018-stroke.html':{t:'<p>To set a shape <i>stroke</i> and <i>stroke width</i> with '+_kjs+', we can set the <em>stroke</em> and <em>strokeWidth</em> properties when we instantiate a shape, or we can use the '+jsdoc_attr('Kinetic.Shape','stroke')+' and '+jsdoc_attr('Kinetic.Shape','strokeWidth')+' methods.</p>'
    ,i:'Mouseover the pentagon to change its stroke color and width.'
    }
  ,'019-opacity.html':{t:'<p>To set a shape <i>opacity</i> with '+_kjs+', we can set the <em>opacity</em> property when we instantiate the shape, or we can use the '+jsdoc_attr('Kinetic.Shape','opacity')+' method.&nbsp; Shapes can have an opacity value between 0 and 1, where 0 is fully transparent, and 1 is fully opaque.&nbsp; Unless otherwise specified, all shapes are defaulted with an opacity value of 1.</p>'
    ,i:' Mouseover the pentagon to change its opacity.'
    }
  ,'020-shadow.html':{t:'<p>To apply <i>shadows</i> with '+_kjs+', we can set the <em>shadowColor</em>, <em>shadowOffset</em>, <em>shadowBlur</em>, and <em>shadowOpacity</em> properties when we instantiate a shape.&nbsp; We can adjust the shadow properties after instantiation by using the '
    +jsdoc_attr('Kinetic.Shape','shadowColor')+', '
    +jsdoc_attr('Kinetic.Shape','shadowOffset')+', '
    +jsdoc_attr('Kinetic.Shape','shadowBlur')+', '
    +jsdoc_attr('Kinetic.Shape','shadowOpacity')
    +' methods.</p>'
    }
  ,'021-linejoin.html':{t:'<p>To set the <i>line join</i> for a shape with '+_kjs+', we can set the <em>lineJoin</em> property when we instantiate a shape, or we can use the '
    +jsdoc_attr('Kinetic.Shape','lineJoin')
    +' method.&nbsp; The <em>lineJoin</em> property can be set to <em>miter</em>, <em>bevel</em>, or <em>round</em>.&nbsp; Unless otherwise specified, the default line join is <em>miter</em>.</p>'
    ,i:'Mouseover the pentagon to change the line join style.'
    }
  ,'022-hideshow.html':{t:'<p>To <i>hide and show a shape</i> with '+_kjs+', we can set the <em>visible</em> property when we instantiate a shape, or we can use the '
    +jsdoc_attr('Kinetic.Shape','hide')+' and '
    +jsdoc_attr('Kinetic.Shape','show')+' methods.</p>'
    ,i:'Click on the buttons to show and hide the shape.'
    }
  ,'023-bindingevents.html':{t:'<p>To <i>detect shape events</i> with '+_kjs+', we can use the '
    +jsdoc_attr('Kinetic.Shape','on')
    +' method to bind event handlers to a node.&nbsp; The '
    +jsdoc_attr('Kinetic.Shape','on')+' method requires an event type and a function to be executed when the event occurs.</p>'
    +kjs_events()
    ,i:'Mouseover and mouseout of the triangle, and mouseover, mouseout, mousedown, and mouseup over the circle.'
    }
  ,'024-imageevents.html':{t:'<p>To <i>only detect events for non transparent pixels in an image</i> with '+_kjs+', we can use the '
    +jsdoc_attr('Kinetic.Line','cache')+' and the '
    +jsdoc_attr('Kinetic.Line','drawHitFromCache')
    +' methods to generate a more precise image hit region.'
    +'&nbsp; By default, events can be triggered for any pixel inside of an image, even if it\'s transparent.'
    +'&nbsp; The '+jsdoc_attr('Kinetic.Line','drawHitFromCache')+' method also accepts an optional argument'
    +' for the alpha channel threshold that determines whether or not a pixel should be drawn onto the hit graph. Must be a value between 0 and 255. The default is 0.</p>'
    ,n:'The <em>createImageHitRegion()</em> method requires that the image is hosted on a web server with the same domain as the code executing it.'
    ,i:'Mouse over the monkey and the lion and observe the mouseover event bindings.&nbsp; Notice that the event is triggered for the monkey if you mouseover any portion of the image, including transparent pixels.&nbsp; Since we created an image hit region for the lion, transparent pixels are ignored, which enables more precise event detection.'
    }
  ,'025-mobileevents.html':{t:'<p>To <i>bind event handlers to shapes on a mobile device</i> with '+_kjs
    +', we can use the '+jsdoc_attr('Kinetic.Shape','on')+' method.'
    +'&nbsp; The '+jsdoc_attr('Kinetic.Shape','on')+' method requires an event type and a function to be executed when the event occurs.</p>'
    +'<p>KineticJS supports <em>touchstart</em>, <em>touchmove</em>, <em>touchend</em>, <em>tap</em>, <em>dbltap</em>, <em>dragstart</em>, <em>dragmove</em>, and <em>dragend</em> mobile events.</p>'
    ,n:'This example only works on iOS and Android mobile devices because it makes use of touch events rather than mouse events.'
    ,i:'move your finger across the triangle to see touch coordinates and touch start and touch end the circle.'
    }
  ,'026-multievent.html':{t:'<p>To <i>bind multiple events to a single handler</i> with '+_kjs
    +', we can use the '+jsdoc_attr('Kinetic.Shape','on')+' method and pass in a space delimited string containing multiple event types.</p>'
    +kjs_events()
    ,i:'Mouseover, mousedown, and mouseup over the circle to observe that the function bound to the circle is executed for each event.'
    }
  ,'027-desktopmobile.html':{t:'<p>To <i>add event handlers</i> to shapes that work for <i>both desktop and mobile applications</i> with '+_kjs
     +', we can use the '+jsdoc_attr('Kinetic.Shape','on')+' method and pass in paired events.'
     +'&nbsp; For example, in order for the mousedown event to be triggered on desktop and mobile applications, we can use the "mousedown touchstart" event pair to cover both mediums.'
     +'&nbsp; In order for the mouseup event to be triggered on both desktop and mobile applications, we can use the "mouseup touchend" event pair.'
     +'&nbsp; We can also use the "dblclick dbltap" event pair to bind a double click event that works for both desktop and mobile devices.</p>'
    +kjs_events()
    ,i:'Mousedown, mouseup, touchstart, or touchend the circle on either a desktop or mobile device to observe the same functionality.'
    }
  ,'028-removeevent.html':{t:'<p>To <i>remove an event listener</i> with '+_kjs
    +', we can use the '+jsdoc_attr('Kinetic.Shape','off')+' method of a shape object which requires an event type such as <em>click</em> or <em>mousedown</em>.</p>'
    +'<p>See also: <small>'
    +jsdoc_attr('Kinetic.Shape','on')
    +'</small></p>'
    ,i:'Click on the circle to see an alert triggered from the onclick event binding.&nbsp; Remove the event listener by clicking on the button and again click on the circle to observe that the event binding has been removed.'
    }
  ,'029-removebyname.html':{t:'<p>To <i>remove an event listener by name</i> with '+_kjs
    +', we can namespace the event type with the '+jsdoc_attr('Kinetic.Shape','on')+' method so that we can later remove the event listener by the same namespace with the '+jsdoc_attr('Kinetic.Shape','off')+' method.</p>'
    ,i:'Click on the circle to see two alerts triggered from two different onclick event bindings.&nbsp; Remove the event listeners using the buttons to the left, and again click on the circle to observe the new onclick bindings.'
    }
  ,'030-customhitregion.html':{t:'<p>To <i>create a custom hit draw function</i> for a shape with '+_kjs
    +', we can set the '+jsdoc_attr('Kinetic.Shape','hitFunc')+' property.'
    +'&nbsp; A hit draw function is the function that KineticJS will use to draw a region used for hit detection.'
    +'&nbsp; Using a custom draw hit function can have several benefits, such as making the hit region larger so that it\'s easier for users to interact with a shape, making some portions of a shape detectable and others not, or simplifying the hit draw function in order to improve rendering performance.</p>'
    ,i:'Mouseover, mouseout, mousedown, and mouseup over the star and observe that the hit region is an over sized circle encompassing the shape.'
    }
  ,'031-listenforevents.html':{t:'<p>To <i>listen or don\'t listen to events</i> with '+_kjs
    +', we can set the <em>listening()</em> property of the config object to <em>true</em> or <em>false</em> when a shape is instantiated, or we can set the listening property with the '+jsdoc_attr('Kinetic.Node','listening')+' method.'
    +'&nbsp; Once we\'ve set the listening property for one or more nodes, we\'ll also need to redraw the hit graph for each affected layer with the '+jsdoc_attr('Kinetic.Layer','drawHit')+' (bad link) method.</p>'
    +'<p>See also: <small>'
    +jsdoc_attr('Kinetic.Stage','drawHit')
    +'</small></p>'
    ,i:'Mouseover the oval to observe that the event handler is not executed.&nbsp; Click on "Listen" to start listening for events and observe that the event handler is now executed.'
    }
  ,'032-cancelpropagation.html':{t:'<p>To <i>cancel event bubble propagation</i> with '+_kjs
    +', we can set the <em>cancelBubble</em> property of the Event object to true.</p>'
    ,i:'Click on the circle to observe that only the circle event binding is handled because the event propagation was canceled when the circle event was triggered, therefore preventing the event object from bubbling upwards.'
    }
  ,'033-eventdelegation.html':{t:'<p>To <i>get the event target</i> with '+_kjs
    +', we can access the <em>target</em> property of the Event object.'
    +'&nbsp; This is particularly useful when using event delegation, in which we can bind an event handler to a parent node, and listen to events that occur on its children.</p>'
    ,i:'Click on the star and observe that the layer event binding correctly identifies the shape that was clicked on.'
    }
  ,'034-fireevents.html':{t:'<p>To <i>fire events</i> with '+_kjs
    +', we can use the '+jsdoc_attr('Kinetic.Node','fire')+' method.'
    +'&nbsp; This enables us to programmatically fire events like click, mouseover, mousemove, etc., and also fire custom events, like foo and bar.</p>'
    }
  ,'035-dragdrop.html':{t:'<p>To <i>drag and drop shapes</i> with '+_kjs
    +', we can set the <em>draggable</em> property to true when we instantiate a shape, or we can use the '+jsdoc_attr('Kinetic.Node','draggable')+' method.'
    +'&nbsp; The '+jsdoc_attr('Kinetic.Node','draggable')+' method enables drag and drop for both desktop and mobile applications automatically.</p>'
    }
  ,'036-dragimage.html':{t:'<p>To <i>drag and drop an image</i> with '+_kjs
    +', we can set the <em>draggable</em> property to true when instantiating an Image, or we can use the '+jsdoc_attr('Kinetic.Node','draggable')+' method.</p>'
    }
  ,'037-draggroup.html':{t:'<p>To <i>drag and drop groups</i> with '+_kjs
    +', we can set the <em>draggable</em> property of the config object to true when the group is instantiated, or we can use the '+jsdoc_attr('Kinetic.Node','draggable')+' method.</p>'
    }
  ,'038-dragline.html':{t:'<p>To <i>drag and drop a line</i> with '+_kjs
    +', we can set the <em>draggable</em> property of the config object to true when the line is instantiated, or we can use the '+jsdoc_attr('Kinetic.Node','draggable')+' method.</p>'
    }
  ,'039-dragstage.html':{t:'<p>To <i>drag and drop the entire stage</i> (pan) with '+_kjs
    +', we can set the <em>draggable</em> property of the config object to true when the stage is instantiated, or we can use the '+jsdoc_attr('Kinetic.Node','draggable')+' method.'
    +'&nbsp; Unlike drag and drop for other nodes, such as shapes, groups, and layers, we can drag the entire stage by dragging any portion of the stage.</p>'
    }
  ,'040-dragevents.html':{t:'<p>To <i>detect drag and drop events</i> with '+_kjs
    +', we can use the '+jsdoc_attr('Kinetic.Shape','on')+' method to bind  <em>dragstart</em>, <em>dragmove</em>, or <em>dragend</em> events to a node.'
    +'&nbsp; The '+jsdoc_attr('Kinetic.Shape','on')+' method requires an event type and a function to be executed when the event occurs.</p>'
    }
  ,'041-simpledragbounds.html':{t:'<p>To <i>restrict the movement of shapes being dragged and dropped</i> with '+_kjs
    +', we can use the <strong>dragBoundFunc</strong> property which is a user defined function that overrides the drag and drop position.'
    +'&nbsp; This function can be used to constrain the drag and drop movement in all kinds of ways, such as constraining the motion horizontally, vertically, diagonally, or radially, or even constrain the node to stay inside of a box, circle, or any other path.</p>'
    +'<p>See also: <small>'
    +jsdoc_attr('Kinetic.Node','draggable')
    +jsdoc_attr('Kinetic.Node','dragBoundFunc')
    +'</small></p>'
    ,i:'Drag and drop the the horizontal text and observe that it can only move horizontally. Drag and drop the vertical text and observe that it can only move vertically.'
    }
  ,'042-complexdragbounds.html':{t:'<p>To <i>bound the movement of nodes being dragged and dropped inside regions</i> with '+_kjs
    +', we can use the <strong>dragBoundFunc</strong> property to define boundaries that the node cannot cross.</p>'
    +'<p>See also: <small>'
    +jsdoc_attr('Kinetic.Node','draggable')
    +jsdoc_attr('Kinetic.Node','dragBoundFunc')
    +'</small></p>'
    ,i:'Drag and drop the the light blue rectangle and observe that it is bound below an imaginary boundary at y = 50.  Drag and drop the yellow rectangle and observe that it is bound inside of an imaginary circle.'
    }
  ,'043-groups.html':{t:'<p>To <i>group multiple shapes together</i> with '+_kjs
    +', we can instantiate a '+jsdocobj('Kinetic.Group')+' object and then add shapes to it with the '+jsdoc_attr('Kinetic.Group','add')+' method.'
    +'&nbsp; Grouping shapes together is really handy when we want to transform multiple shapes together, e.g. if we want to move, rotate, or scale multiple shapes at once.'
    +'&nbsp; Groups can also be added to other groups to create more complex Node trees.</p>'
    }
  ,'044-layering.html':{t:'<p>To <i>layer shapes</i> with '+_kjs
    +', we can use one of the following layering methods: '
    +jsdoc_attr('Kinetic.Node','moveToTop')+', '
    +jsdoc_attr('Kinetic.Node','moveToBottom')+', '
    +jsdoc_attr('Kinetic.Node','moveUp')+', '
    +jsdoc_attr('Kinetic.Node','moveDown')+', '
    +jsdoc_attr('Kinetic.Node','setZIndex')+'.'
    +'&nbsp; You can also layer groups and layers.</p>'
    ,i:'Drag and drop the boxes to move them around, and then use the buttons on the left to reorder the yellow box.'
    }
  ,'045-changecontainers.html':{t:'<p>To <i>move a shape from one container into another</i> with '+_kjs
    +', we can use the '+jsdoc_attr('Kinetic.Node','moveTo')+' method which requires a container as a parameter.'
    +'&nbsp; A container can be another stage, a layer, or a group.'
    +'&nbsp; You can also move groups into other groups and layers, or shapes from groups directly into other layers.</p>'
    +'<p>See also: <small>'
    +jsdocobj('Kinetic.Stage')+', '
    +jsdocobj('Kinetic.Group')+', '
    +jsdocobj('Kinetic.Layer')+', '
    +jsdocobj('Kinetic.Shape')+', '
    +jsdocobj('Kinetic.Node')+' '
    +'</small></p>'
    ,i:'Drag and drop the groups and observe that the red rectangle is bound to either the yellow group or the blue group.&nbsp; Use the buttons on the left to move the box from one group into another.'
    }
  ,'046-simpletransition.html':{t:'<p>To <i>create a linear transition</i> with '+_kjs
    +', we can use a '+jsdocobj('Kinetic.Tween')+' instance to transition a set of properties from one state to another.'
    +'&nbsp; By default, the transition easing is '+jsdoc_attr_s('Kinetic.Easings','Linear')+'.'
    +'&nbsp; Any <b>numeric</b> property of a Shape, Group, Layer, or Stage can be transitioned, such as <em>x</em>, <em>y</em>, <em>rotation</em>, <em>width</em>, <em>height</em>, <em>radius</em>, <em>strokeWidth</em>, <em>opacity</em>, <em>scale</em>, <em>offset</em>, etc.'
    +'&nbsp; The config passed into the '+jsdocobj('Kinetic.Tween')+' instance must also include a <em>duration</em> in seconds which defines how long it takes for the transition to occur.</p>'
    +'<p>See also: <small>'
    +jsdocobj('Kinetic.Stage')+', '
    +jsdocobj('Kinetic.Group')+', '
    +jsdocobj('Kinetic.Layer')+', '
    +jsdocobj('Kinetic.Shape')+' '
    +'</small></p>'
    ,w:'<code>transitionTo()</code> is not supported in v5.1.1, using '+jsdocobj('Kinetic.Tween')+' instead.'
    }
  ,'047-transitioneasing.html':{t:'<p>To <i>use a simple non-linear easing function</i> with '+_kjs
    +', we can set the <em>easing</em> property of a '+jsdocobj('Kinetic.Tween')+' config object to one of the '
    +jsdocobj('Kinetic.Easings')+'.'
    +'&nbsp; In this tutorial, we\'ll use'
    +' an '+jsdoc_attr_s('Kinetic.Easings','EaseIn')+' easing for the green rectangle,'
    +' an '+jsdoc_attr_s('Kinetic.Easings','EaseOut')+' easing for the blue rectangle, and'
    +' an '+jsdoc_attr_s('Kinetic.Easings','EaseInOut')+' easing for the red rectangle.'
    +'&nbsp; This tutorial transitions the scale of a rectangle when you mouse over it.</p>'
    ,i:'Mouseover or touchstart the boxes to transition them with different easing functions'
    ,w:'<code>transitionTo()</code> is not supported in v5.1.1, using '+jsdocobj('Kinetic.Tween')+' instead.'
    }
  ,'048-alleasing.html':{t:'<p>To <i>use any of the easing functions</i> with '+_kjs
    +', we can set the <em>easing</em> property of each of the '+jsdocobj('Kinetic.Tween')+' config objects to each of the '
    +jsdocobj('Kinetic.Easings')+'.'
    +'</p>'
    ,i:'Press "Go!" to transition all of the text nodes.&nbsp; Press "Reset" to reset them back to their original position.'
    ,w:'<code>transitionTo()</code> is not supported in v5.1.1, using '+jsdocobj('Kinetic.Tween')+' instead.'}
  ,'049-callback.html':{t:'<p>To <i>define a callback for a transition</i> with '+_kjs
    +', we can set the <em>onFinish</em> property of a '+jsdocobj('Kinetic.Tween')+' config object.'
    +'</p>'
    ,w:'<code>transitionTo()</code> is not supported in v5.1.1, using '+jsdocobj('Kinetic.Tween')+' instead. In v5.1.1 use <em>onFinish</em> property instead of <em>callback</em> in tween.'}
  ,'050-stopresume.html':{t:'<p>To <i>stop and resume a transition</i> with '+_kjs
    +', we can use the '+jsdoc_attr('Kinetic.Tween','pause')+' and '+jsdoc_attr('Kinetic.Tween','play')+' methods of a '+jsdocobj('Kinetic.Tween')+' object.</p>'
    ,w:'<code>transitionTo()</code> is not supported in v5.1.1, using '+jsdocobj('Kinetic.Tween')+' instead.'}
  ,'051-createanimation.html':{t:'<p>To <i>create custom animations</i> with '+_kjs
    +', we can use the '+jsdocobj('Kinetic.Animation')+' constructor which takes two arguments, the required update function and an optional layer that will be updated with each animation frame.'
    +'&nbsp; The animation function is passed a frame object which contains'
    +' a <em>time</em> property which is the number of milliseconds that the animation has been running,'
    +' a <em>timeDiff</em> property which is the number of milliseconds that have passed since the last frame, and'
    +' a <em>frameRate</em> property which is the current frame rate in frames per second.</p>'
    +'<p>The <b>update function should never redraw the stage or the layer</b> because the animation engine will intelligently handle that for us.'
    +'&nbsp; The update function should only contain logic that updates Node properties, such as'
    +' <em>position</em>,'
    +' <em>rotation</em>,'
    +' <em>scale</em>,'
    +' <em>width</em>,'
    +' <em>height</em>,'
    +' <em>radius</em>,'
    +' <em>colors</em>, etc.'
    +'&nbsp; Once the animation has been created, we can start it at anytime with the '+jsdoc_attr('Kinetic.Animation','start')+' method.</p>'
    +'<p>To improve animation performance, it\'s highly recommended that we pass in the layer argument so that the animation engine knows which layer to redraw.'
    +'&nbsp; If the second argument is not provided, nothing will be redrawn, and if the second argument is a reference to the stage, then all layers in the stage will be drawn.</p>'}
  ,'052-moving.html':{t:'<p>To <i>animate a shape\'s position</i> with '+_kjs
    +', we can create a new animation with '+jsdocobj('Kinetic.Animation')+' which modifies the shape\'s position with each animation frame.</p>'
    }
  ,'053-rotating.html':{t:'<p>To <i>animate a shape\'s rotation</i> with '+_kjs
    +', we can create a new animation with '+jsdocobj('Kinetic.Animation')+', and define a function which modifies the shape\'s rotation with each animation frame.</p>'
    +'<p>In this tutorial, we\'ll rotate a blue rectangle about the top left corner, a yellow rectangle about its center, and a red rectangle about an outside point.</p>'
    }
  ,'054-scaling.html':{t:'<p>To <i>animate a shape\'s</i> scale with '+_kjs
    +', we can create a new animation with '+jsdocobj('Kinetic.Animation')+', and define a function which modifies the shape\'s scale with each animation frame.</p>'
    +'<p>In this tutorial, we\'ll scale the <em>x</em> and <em>y</em> component of a blue hexagon, the <em>y</em> component of a yellow hexagon, and the <em>x</em> component of a red hexagon about an axis positioned on the right side of the shape.</p>'
    ,i:'drag and drop the hexagons as they animate'
    }
  ,'055-stopanimation.html':{t:'<p>To <i>stop an animation</i> with '+_kjs
    +', we can use the '+jsdoc_attr('Kinetic.Animation','stop')+' method.'
    +'&nbsp; To restart the animation, we can again call the '+jsdoc_attr('Kinetic.Animation','start')+'.</p>'
    ,i:'Click on "Start" to start the animation and "Stop" to stop the animation.'
    }
  ,'056-selectbyid.html':{t:'<p>To <i>select a shape by id</i> with '+_kjs
    +', we can use the &lt;node&gt;.<em>get()</em> method and pass a <em>#</em> selector.'
    +'&nbsp; The &lt;node&gt;.<em>get()</em> method always returns an array of elements, even if we are expecting it to return one element.'
    +'&nbsp; The &lt;node&gt;.<em>get()</em> method works for any node, including the stage, layers, groups, and shapes.</p>'
    +'<p>The return array is a '+jsdocobj('Kinetic.Collection')+' array, which is basically a typical JavaScript array with a special <em>each()</em> method.</p>'
    ,i:'press the "Activate Rectangle" button to select the rectangle by id and perform a transition.&nbsp; You can also drag and drop the rectangle.'
    }
  ,'057-selectbyname.html':{t:'<p>To <i>select shapes by name</i> with '+_kjs
    +', we can use the &lt;node&gt;.<em>get()</em> method using the <em>.</em> selector.'
    +'&nbsp; The &lt;node&gt;.<em>get()</em> method returns an array of nodes that match the selector string.'
    +'&nbsp; The return array is a '+jsdocobj('Kinetic.Collection')+' array, which is basically a typical JavaScript array with a special <em>each()</em> method.</p>'
    +'<p>The <em>each()</em> method enables us to quickly iterate through every node in the array.&nbsp; We can also directly use <em>on()</em> and <em>off()</em> to add or remove event listeners to every node in the array.</p>'
    ,i:'press the "Activate Rectangles" button to select the rectangles by name and perform a transition on each.&nbsp; You can also drag and drop the rectangles.'
    }
  ,'058-dataurl.html':{t:'<p>To <i>get the data URL of the stage</i> with '+_kjs
    +', we can use the '+jsdoc_attr('Kinetic.Node','toDataURL')+' method which requires a callback function.'
    +'&nbsp; In addition, we can also pass in a mime type such as <em>image/jpeg</em> and a quality value that ranges between 0 and 1.'
    +'&nbsp; We can also get the data URLs of specific nodes, including layers, groups, and shapes.</p>'
    ,n:'The '+jsdoc_attr('Kinetic.Node','toDataURL')+' method requires that any images drawn onto the canvas are hosted on a web server with the same domain as the code executing it.'
    +'&nbsp; If this condition is not met, a <em>SECURITY_ERR</em> exception is thrown.'
    ,i:'Drag and drop the rectangle and then click on the save button to get the composite data url and open the resulting image in a new window'
    }
  ,'059-serializestage.html':{t:'<p>To <i>save the stage as a JSON string</i> with '+_kjs
    +', we can use the '+jsdoc_attr('Kinetic.Node','toJSON')+' method which serializes the KineticJS Node tree into text which can be saved in web storage or in an offline database.'
    +'&nbsp; We can also serialize other nodes, including layers, groups, and shapes.</p>'
    }
  ,'060-simpleload.html':{t:'<p>To <i>deserialize a JSON string</i> with '+_kjs
    +', we can use the '+jsdoc_attr_s('Kinetic.Node','create')+' method which creates a node from a JSON string.'
    +'&nbsp; If we want to deserialize a stage node, we can also pass in an optional container parameter.</p>'
    }
  ,'061-complexload.html':{t:'<p>To <i>load a complex stage that originally contained images and event bindings</i> using '+_kjs
    +', we need to create a stage node using '+jsdoc_attr_s('Kinetic.Node','create')+', and then set the images and event handlers with the help of selectors using the <em>get()</em> method.'
    +'&nbsp; Images and event handlers must be manually set because they aren\'t serializable.</p>'
    }
  ,'062-shapecache.html':{t:'<p>One way to <i>drastically improve drawing performance</i> for complex '+_kjs
    +' shapes is to cache them as images.'
    +'&nbsp; This can be achieved by using the '+jsdoc_attr_s('Kinetic.Node','toImage')+' method to convert a node into an image object, and then instantiating a new '+jsdocobj('Kinetic.Image')+' shape with the image object.</p>'
    +'<p>This particular tutorial of drawing 10 cached stars rather than drawing 10 individual stars sees about a 4x drawing performance boost.'
    +'&nbsp; Caching can be applied to any node, including the stage, layers, groups, and shapes.</p>'
    ,n:'The '+jsdoc_attr_s('Kinetic.Node','toImage')+' method requires that the image is hosted on a web server with the same domain as the code executing it.'
    }
};

var locc=location.href.split('/').reverse()[0]

var s='';
var cc='';
for(let x=0;x<menu.length;x++) {
  if(menu[x].c!==cc) {
    cc=menu[x].c;
    s+='<h3>'+cc+'</h3>';
  }
  var act='';
  if(menu[x].u===locc) {
    act=' class="active"';
  }
  s+='<a href="'+menu[x].u+'"'+act+'>'+menu[x].t+'</a>';
}
s+="<br><br>";
document.getElementById('menu').innerHTML=s;

function titleFromUrl(u){
  for(var i in menu) {
    if(menu[i].u===u) {
      return menu[i].t;
    }
  }
}
function nextprev(){
  var idx=locc.split('-')[0];
  idx=parseInt(idx);
  console.log('idx',idx);
  var pi;
  var ni;
  var ct=0;
  var fl=false;
  var fl2=false;
  var fi;
  var li;
  for(var i in docs) {
    if(fl===false) {
      var fi=i;
      fl=true;
    }
    ct++;
    li=i;
  }
  fl=false;

  for(var i in docs) {
    var x=parseInt(i.split('-')[0]);
    if(fl) {
      if(ni===undefined) {
        ni=i;
      }
    }else{
      if(x!==idx) {
        pi=i;
      }
    }
    if(x===idx) {
      fl=true;
    }
  }
  if(pi===undefined) {
    pi=li;
  }
  if(ni===undefined) {
    ni=fi;
  }
  //console.log('ni',ni,'pi',pi,'fi',fi,'li',li);
  var s='';
  s+='<a href="'+pi+'" title="'+titleFromUrl(pi)+'">'+'prev'+'</a>&nbsp;';
  s+='<a href="'+ni+'" title="'+titleFromUrl(ni)+'">'+'next'+'</a>';
  return s;
}

document.getElementById('info').innerHTML='<div id="tit">'+titleFromUrl(locc)+'</div><div id="nextprev"></div>'+docs_str(locc)+"<br><br><br><br><br><br>";
document.getElementById('nextprev').innerHTML=nextprev();


